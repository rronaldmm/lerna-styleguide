# lerna monorepo

This project contains 3 packages: ***components***, ***styleguide*** and ***project***.

Both ***styleguide*** and ***project*** have a peerDependency on ***components***, which contains 'all the React components'.

***styleguide*** exposes the components in a Storybook-project as a documentation and showcase in one.

***project*** is the 'actual' website which uses the components.

### Get started
To work with this monorepo, you must run `lerna bootstrap` after you have checked out the code. This command runs yarn install for all *external* dependencies of each package, and links the internal dependencies using a Symlinks.
Make sure you have lerna installed globally (`yarn add lerna -G`)

### Edit components
To edit components, just make the desired changes in ***components***, don't forget to document your changes in ***styleguide***. 
When you are done, `yarn build` the components, and run `lerna bootstrap` again to update your work to all packages. It *should* not matter from which package you run this command.

### project & storybook
Both packages will run with `yarn start`, both will build a production-ready version with `yarn build`  


## TODO:
 - proptypes and description don't work properly yet 