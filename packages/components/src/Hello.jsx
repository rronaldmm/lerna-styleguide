import React from 'react'
import * as PropTypes from 'prop-types'

const Hello = ({ name }) => (
  <div>Hello { name }</div>
)

Hello.propTypes = {
  /** name to display*/
  name: PropTypes.string
}
Hello.defaultProps = {
  name: 'world'
}

export default Hello