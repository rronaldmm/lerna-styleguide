import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import { withKnobs } from '@storybook/addon-knobs'
import Another from 'components/lib/Another'

Another.displayName = 'Another'

storiesOf('Another', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .addParameters({
    info: {
      header: false,
      inline: true
    }
  })
  .add('another', () => (
    <Another/>
  ), {
    info: {
      text: 'example'
    }
  })
  .add('two', () => (
    <Another/>
  ), {
    info: {
      text: 'example two'
    }
  })