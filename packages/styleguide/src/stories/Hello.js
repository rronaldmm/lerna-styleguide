import React from 'react'
import { storiesOf } from '@storybook/react'
import { withInfo } from '@storybook/addon-info'
import {
  text,
  withKnobs
} from '@storybook/addon-knobs'
import Hello from 'components/lib/Hello'

Hello.displayName = 'Hello'

storiesOf('Hello', module)
  .addDecorator(withKnobs)
  .addDecorator(withInfo)
  .addParameters({
    info: {
      header: false,
      inline: true
    }
  })
  .add('world', () => (
    <Hello
      name="world"
    />
  ), {
    info: {
      text: 'hello world example'
    }
  })
  .add('knob', () => (
    <Hello
      name={ text('text','world')}
    />
  ), {
    info: {
      text: 'example using knobs'
    }
  })